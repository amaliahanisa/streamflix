import React, { useEffect, useState }  from 'react';
import './style.css';
import { Link } from 'react-router-dom'
import BeenhereIcon from '@material-ui/icons/Beenhere';

function  Movie({id, title, poster_path, overview, vote_average}) {

  const IMG_API = "https://image.tmdb.org/t/p/w300/"
  const [price, setPrice] = useState(0);
  const [isBought, setIsBought] = useState('false');

  useEffect(() => {
      getPrice();
      getIsBought();
  }, [price])

  const getPrice = () =>{
      var vote = parseInt(vote_average);
      if(vote >= 1 && vote < 3 ){
        setPrice(3500);
      }
      else if(vote >= 3 && vote < 6 ){
        setPrice(8250);
      }
      else if(vote >= 6 && vote < 8 ){
        setPrice(16350);
      }
      else if(vote >= 8 && vote < 10 ){
        setPrice(21250);
      }
      localStorage.setItem('price'+(id), price);
  }

  const getIsBought = () => {
    setIsBought(localStorage.getItem('isBought'+(id)));
  }

  return (
        <div className="movie">
            <Link className="movie-link" to={`/${id}-${title.replace(/\s+/g, '-')}`}>
              <div> 
              <img className="movie-img" src={IMG_API + poster_path}></img>
            </div>
            <div className="movie-info">
                <h5>{title}</h5>
                <span>{vote_average}</span>
                <span>Rp {price}</span>
                {isBought ==='true' && <BeenhereIcon/>}
            </div>
            <div className="movie-over" >
                <h4>Overview:</h4>
                <p>{overview}</p>
            </div>
            </Link>
            
        </div>
  );
}

export default Movie;
