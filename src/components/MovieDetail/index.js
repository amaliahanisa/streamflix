import React, { useEffect, useState }  from 'react';
import './style.css';
import BeenhereIcon from '@material-ui/icons/Beenhere';
import Movie from '../Movie/index';

function MovieDetail({ match }) {

    const [movie, setMovie] = useState([]);
    const [casts, setCasts] = useState([]);
    const [recomendations, setRecomendations] = useState([]);
    const [similars, setSimilars] = useState([]);
    const [isBought, setIsBought] = useState('false');
    const [isEnoughBalance, setIsEnoughBalance] = useState(true);

    useEffect(() => {
        getMovie();
        getCasts();
        getRecomendations();
        getSimilars();
        window.scrollTo(0,0);
    },[match]);


  const IMG_API = "https://image.tmdb.org/t/p/w500/"

  const getMovie= async () => {
      const data = await fetch(`https://api.themoviedb.org/3/movie/${match.params.id}?api_key=66651f0cc194ee1cd3bae18a75658d04`);
      const movie = await data.json();
      console.log(movie)
      setMovie(movie);
      setIsBought(localStorage.getItem('isBought'+(movie.id)));
      setIsEnoughBalance(parseInt(localStorage.getItem('balance')) > parseInt(localStorage.getItem('price'+ (movie.id))))
  }

  const getCasts = async () =>{
    const data = await fetch(`https://api.themoviedb.org/3/movie/${match.params.id}/credits?api_key=66651f0cc194ee1cd3bae18a75658d04&language=en-US`)
    const casts = await data.json();
    setCasts(casts.cast)
    console.log(casts)
  }

  const getRecomendations = async () =>{
    const data = await fetch(`https://api.themoviedb.org/3/movie/${match.params.id}/recommendations?api_key=66651f0cc194ee1cd3bae18a75658d04&language=en-US&page=1`)
    const recomendations = await data.json();
    setRecomendations(recomendations.results)
    console.log(recomendations)
  }

  const getSimilars= async () =>{
    const data = await fetch(`https://api.themoviedb.org/3/movie/${match.params.id}/similar?api_key=66651f0cc194ee1cd3bae18a75658d04&language=en-US&page=1`)
    const similars = await data.json();
    setSimilars(similars.results)
    console.log(similars)
  }


  const calculateBalance = () =>{
        var currentBalance = localStorage.getItem('balance') - localStorage.getItem('price'+(movie.id));
        console.log(currentBalance);
        localStorage.setItem('isBought'+(movie.id), 'true');
        setIsBought(localStorage.getItem('isBought'+(movie.id)));
        localStorage.setItem('balance', currentBalance);
        console.log(localStorage.getItem('balance'));
  }

  const handleOnCLick = (event) =>{
        calculateBalance();
  }


  return (
      <div >
          <div className="header-div">
            <h3>Saldo : Rp { localStorage.getItem('balance')}</h3>
         </div>
            <div className="movie-div">
                 <h1>{movie.original_title}</h1>
                 <h3>{movie.vote_average}</h3>
                 <img src={IMG_API + movie.poster_path} alt={movie.origin_title}></img>
                 <h3>Rp {localStorage.getItem('price'+(movie.id))}</h3>
                 {isBought === 'true' ? <BeenhereIcon/>  : <button onClick={handleOnCLick} disabled={!isEnoughBalance}> Buy</button>}
                 <p>{movie.overview}</p>
                 <h4>Duration: {movie.runtime} minutes</h4>
                 <h3>Cast :</h3>
                 <ul>
                 { casts.map((cast) => 
                    <li className="cast-list "key={cast.id}>{cast.name}</li>
                 )}
                 </ul>
            </div>
            <div className="header-div">
            <h1>Recomendations</h1>
            </div>
            <div className="movies-container">
            {recomendations.map((movie) => 
                <Movie key={movie.id} {...movie}/>
            )}
            </div>

            <div className="header-div">
            <h1>Similar</h1>
            </div>
            <div className="movies-container">
            {similars.map((movie) => 
                <Movie key={movie.id} {...movie}/>
            )}
            </div>
             
      </div>
  
  );
}

export default MovieDetail;
