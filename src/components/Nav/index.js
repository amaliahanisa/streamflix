import React from 'react';
import './style.css';
import { Link } from 'react-router-dom';


function Nav() {
  return (
    <nav>
        <Link className="nav-link" to='/'><h3>StreamFlix</h3></Link>
        <ul className="nav-links">
        </ul>
    </nav>
  );
}

export default Nav;
