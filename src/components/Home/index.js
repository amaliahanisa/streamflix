import React, { useEffect, useState }  from 'react';
import './style.css';
import Movie from '../Movie/index';
import Pagination from '@material-ui/lab/Pagination';
import Typography from '@material-ui/core/Typography';
import { useHistory } from "react-router-dom";


function Home({ match }) {

  const [balance, setBalance] = useState(100000);
  const [movies, setMovies] = useState([]);
  const history = useHistory();

  useEffect(() => {
    getMovies();
    getBalance();
  }, [])

  const getMovies = async () => {
    const data = await fetch(`https://api.themoviedb.org/3/movie/now_playing?api_key=66651f0cc194ee1cd3bae18a75658d04&region=ID&page=${match.params.index}`);
    const movies = await data.json();
    console.log(movies.results);
    setMovies(movies.results);
  }

  const handlePageChange = (event, value)=>{
    history.push(`/page=${value}`);
    window.location.reload(); 
  }

  const getBalance = () => {
    if(localStorage.getItem('balance') == null){
      localStorage.setItem('balance', 100000)
      console.log('yessnulll')
    }
    setBalance(localStorage.getItem('balance'));
    console.log(balance)
  }

  return (
      <div>
        <div className="header-div">
        <h3>Saldo: Rp {balance}</h3>
        <h1>Now Playing in Indonesia</h1>
        </div>
          <div className="movies-container">
            {movies.map((movie) => 
            <Movie key={movie.id} {...movie}/>
            )}
          </div>
          <div className="pagination-div">
          <Typography>Page: {match.params.index}</Typography>
            <Pagination count={20} page={match.params.index} onChange={handlePageChange}/>
          </div>
          
      </div>
  );
}

export default Home;
