import './App.css';
import Nav from './components/Nav/index';
import MovieDetail from './components/MovieDetail/index';
import Home from './components/Home/index';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';

function App() {

  return (
    <Router>
    <div className="App">
      <Nav />
      <Switch>
        <Route path="/:id-:slug" exact component={MovieDetail} />
        <Route path="/page=:index" exact component={Home} />
        <Route path="/" exact >
          <Redirect to="/page=1" />
        </Route>
      </Switch>
    </div>
    </Router>

  )
}

export default App;
